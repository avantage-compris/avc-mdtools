# avc-mdtools

Tools that generate HTML documents from .md (Markdown) files.

## Usage

### 1. Single file

To convert a single .md file into a XHTML fragment:

	MarkdownUtils.convertMdToXHTML(mdFile)

or:

	MarkdownUtils.convertMdToXHTML(mdContent)
	 
### 2. HTML site

To convert a list of .md files into a static HTML site:

	MarkdownUtils.convertMdToHTMLSite(destDir, mdFiles)

This method launches the default behaviour,
which uses the default XSLT, default-md2site.html.xsl
(generally this file is embedded within the JAR file).

#### 2.1. Custom XSLT sheet
	
You may want to apply your own XSLT sheet, in that case, use the following
syntax:

    final MarkdownFiles mdFiles = new MarkdownFiles(rootDir);

    mdFiles.addFile(mdFile1);
    mdFiles.addFile(mdFile2);
    ...
		
    final MarkdownToHTMLConverter converter = new MarkdownToHTMLConverter();

    converter.setTransformationSheet(xsltFile);
		
    converter.convertToHTMLSite(mdFiles, destDir);

Your custom XSLT sheet will be applied to a big XML document that
contains all XHTML fragments, enclosed in a 
\<mdtools:page\> block with metadata about the conversion.

This big XML document is useful if you want to implement breadcrumbs and what.

The XSLT sheet will be passed a “path” parameter, which corresponds
to the file identifier in the list of original .md files.

#### 2.2. Custom CSS stylesheet

You may want to apply your own CSS stylesheet
for the generated HTML pages, in that case, use the following
syntax:
		
    final MarkdownToHTMLConverter converter = new MarkdownToHTMLConverter();

    converter.setCssStylesheet(cssFile);
		
    converter.convertToHTMLSite(mdFiles, destDir);

Your custom XSLT sheet will be applied to a big XML document that
contains all XHTML fragments, enclosed in a 
\<mdtools:page\> block with metadata about the conversion.

This big XML document is useful if you want to implement breadcrumbs and what.

The XSLT sheet will be passed a “path” parameter, which corresponds
to the file identifier in the list of original .md files.
	
#### 2.3. Convert “README.md” into “index.html”

Use the following syntax:

    final MarkdownFiles mdFiles = new MarkdownFiles(rootDir);

    mdFiles.addFile(new File("README.md"));
    
    mdFiles.getFileDesc("README.md").setHtmlOutputPath("index.html");

#### 2.4. Generate a Table of Contents (toc)

Use the following syntax:

    final MarkdownFiles mdFiles = new MarkdownFiles(rootDir);

    mdFiles.addFile(new File("README.md"));
    
    mdFiles.getFileDesc("README.md").appendContent("## This is my Table of Contents");
	mdFiles.getFileDesc("README.md").appendToc();
