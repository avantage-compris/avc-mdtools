# Formats

Italics: _This_ is emphasized.

Italics: *This* is too.
 
Bold: **This** is bold.