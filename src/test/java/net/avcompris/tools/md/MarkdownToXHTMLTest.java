package net.avcompris.tools.md;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.tools.md.MarkdownUtils.convertMdToXHTML;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.InputSource;

public class MarkdownToXHTMLTest {

	@Before
	public void setUp() throws Exception {

		xhtml = null;
	}

	@After
	public void checkXHTMLisWellFormed() throws Exception {

		DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(new InputSource(new StringReader("<div>" + xhtml
						+ "</div>")));
	}

	private String xhtml = null;

	@Test
	public void testHelloWorld() throws Exception {

		xhtml = convertMdToXHTML(new File("src/test/markdown", "HelloWorld.md"));

		assertContains(xhtml, "<h1>Hello World!</h1>");
		assertContains(xhtml, "<p>Lorem ipsum ");
		assertContains(xhtml, " ultricies consequat.</p>");
	}

	@Test
	public void testBlog_a() throws Exception {

		xhtml = convertMdToXHTML(new File("src/test/markdown",
				"blog/20170703-a.md"));

		assertContains(xhtml, "<h3>Monday, July 3rd</h3>");
		assertContains(xhtml, "<p>Lorem ipsum.</p>");
	}

	@Test
	public void testBlog_b() throws Exception {

		xhtml = convertMdToXHTML(new File("src/test/markdown",
				"blog/20170704-b.md"));

		assertContains(xhtml, "<h4>Tuesday, July 4th</h4>");
	}

	private static void assertContains(final String text, final String needle) {

		checkNotNull(text, "text");
		checkNotNull(needle, "needle");

		assertTrue("Text should contain: \"" + needle + "\"",
				text.contains(needle));
	}

	@Test
	public void testREADME() throws Exception {

		xhtml = convertMdToXHTML(new File("README.md"));

		// assertContains(xhtml, "<h4>Tuesday, July 4th</h4>");
	}

	@Test
	public void testREADME_20170722() throws Exception {

		xhtml = convertMdToXHTML(new File(
				"src/test/markdown/README_20170722.md"));

		assertContains(xhtml, "&lt;mdtools:page&gt;");
	}

	@Test
	public void testTable() throws Exception {

		xhtml = convertMdToXHTML(new File("src/test/markdown", "Table.md"));

		assertContains(xhtml, "<table>");
	}
}
