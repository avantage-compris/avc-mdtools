package net.avcompris.tools.md;

import static net.avcompris.tools.md.MarkdownUtils.convertMdToHTMLFrameset;

import java.io.File;

import org.junit.Test;

public class MarkdownToHTMLFramesetTest {

	@Test
	public void testBlog() throws Exception {

		convertMdToHTMLFrameset(new File("target", "blog_frameset"), new File(
				"src/test/markdown", "HelloWorld.md"), new File(
				"src/test/markdown", "blog"));
	}
}
