package net.avcompris.tools.md;

import static net.avcompris.tools.md.MarkdownUtils.convertMdToHTMLSite;

import java.io.File;

import org.junit.Test;

public class MarkdownToHTMLSiteTest {

	@Test
	public void testBlog() throws Exception {

		convertMdToHTMLSite(new File("target", "blog"), new File(
				"src/test/markdown", "HelloWorld.md"), new File(
				"src/test/markdown", "blog"));
	}
}
