package net.avcompris.tools.md;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.tools.md.MarkdownUtils.convertMdToConfluenceMarkup;
import static org.apache.commons.lang3.StringUtils.splitPreserveAllTokens;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

public class MarkdownToConfluenceMarkupTest {

	@Test
	public void testHelloWorld() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "HelloWorld.md"));

		final String[] lines = splitPreserveAllTokens(wiki, "\r\n");

		assertEquals(5, lines.length);
		assertEquals("h1. Hello World!", lines[0]);
		assertEquals("", lines[1]);
		assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit."
				+ " Praesent tempus dui quis purus ultricies consequat.",
				lines[2]);
		assertEquals("", lines[3]);
		assertEquals("Link to [B|blog/20170704-b.md]", lines[4]);
	}

	@Test
	public void testBlog_a() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "blog/20170703-a.md"));

		assertContains(wiki, "h3. Monday, July 3rd");
		assertContains(wiki, "Lorem ipsum.");
	}

	@Test
	public void testBlog_b() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "blog/20170704-b.md"));

		assertContains(wiki, "h4. Tuesday, July 4th");
	}

	private static void assertContains(final String text, final String needle) {

		checkNotNull(text, "text");
		checkNotNull(needle, "needle");

		assertTrue("Text should contain: \"" + needle + "\"",
				text.contains(needle));
	}

	@Test
	public void testREADME() throws Exception {

		convertMdToConfluenceMarkup(new File("README.md"));
	}

	@Test
	public void testREADME_20170722() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown/README_20170722.md"));

		final String[] lines = splitPreserveAllTokens(wiki, "\r\n");

		// assertEquals(15, lines.length);
		assertEquals("h1. avc-mdtools", lines[0]);
		assertEquals("h2. Usage", lines[4]);
		assertEquals("h3. 1. Single file", lines[6]);
		assertEquals("To convert a single .md file into a XHTML fragment:",
				lines[8]);
		assertEquals("{code}", lines[10]);
		assertEquals("MarkdownUtils.convertMdToXHTML(mdFile)", lines[11]);
		assertEquals("{code}", lines[12]);
		assertEquals("or:", lines[14]);
		assertEquals("{code}", lines[16]);
		assertEquals("MarkdownUtils.convertMdToXHTML(mdContent)", lines[17]);
		assertEquals("{code}", lines[18]);
		assertEquals("h4. 2.1. Custom XSLT sheet", lines[30]);
	}

	@Test
	public void testTable() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "Table.md"));

		final String[] lines = splitPreserveAllTokens(wiki, "\r\n");

		assertContains(wiki, "|| Tables || Are || Cool");
		assertContains(wiki, "| col 3 is | right-aligned | $1600");
		assertContains(wiki, "| col 2 is | centered | $12");

		assertEquals("There.", lines[9]);

		assertEquals(10, lines.length);
	}

	@Test
	public void testCode() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "Code.md"));

		assertEquals("This is {{some}} code.", wiki);
	}

	@Test
	public void testFormats() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "Formats.md"));

		assertContains(wiki, "Italics: _This_ is emphasized.");
		assertContains(wiki, "Italics: _This_ is too.");
		assertContains(wiki, "Bold: *This* is bold.");
	}

	@Test
	public void testLists() throws Exception {

		final String wiki = convertMdToConfluenceMarkup(new File(
				"src/test/markdown", "Lists.md"));

		assertContains(wiki, "* First item");

		final String[] lines = splitPreserveAllTokens(wiki, "\r\n");

		assertEquals("* First item", lines[4]);
		assertEquals("* Second item", lines[5]);

		assertEquals("# Procedure", lines[9]);
		assertEquals("# Step", lines[10]);
		assertEquals("#* What do you think", lines[11]);
		assertEquals("#* of this?", lines[12]);
		assertEquals("# More", lines[13]);
		assertEquals("#* What do you think", lines[14]);
		assertEquals("#* of this,", lines[15]);
		assertEquals("#*# My", lines[16]);
		assertEquals("#*# Friend", lines[17]);
		assertEquals("# End", lines[18]);
	}
}
