package net.avcompris.tools.md;

import java.io.IOException;
import java.io.InputStream;

public class MarkdownToHTMLFramesetConverter extends
		AbstractMarkdownToXxxConverter {

	public MarkdownToHTMLFramesetConverter() {
		
		super(true);
	}

	@Override
	protected InputStream getDefaultXSLTStream() throws IOException {

		return getResourceAsStream("xslt/default-md2site-frameset.html.xsl");
	}
}
