package net.avcompris.tools.md;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;

import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.TransformUtils;
import com.github.rjeschke.txtmark.Configuration;
import com.github.rjeschke.txtmark.Processor;

public abstract class MarkdownUtils {

	/**
	 * Convert a Markdown stream into a XHTML stream, using txtmark.
	 */
	public static String convertMdToXHTML(final File markdownFile)
			throws IOException {

		final String markdownContent = FileUtils.readFileToString(markdownFile,
				UTF_8);

		return convertMdToXHTML(markdownContent);
	}

	/**
	 * Convert a Markdown stream into a XHTML stream, using txtmark.
	 */
	public static String convertMdToXHTML(final String markdownContent)
			throws IOException {

		final String prep = markdownContent //
				.replace("\\<", "&lt;") //
				.replace("\\>", "&gt;");

		// CONVERSION

		final Configuration configuration = // Configuration.DEFAULT_SAFE;
		Configuration.builder().setEncoding(UTF_8).setSafeMode(true)
				.enableParseTable().build();

		final String processed = Processor.process(prep, configuration);

		// 9. END

		// System.out.println(processed);

		return processed;
	}

	/**
	 * Convert a Markdown stream into a Confluence-markup stream.
	 */
	public static String convertMdToConfluenceMarkup(
			final String markdownContent) throws IOException,
			TransformerException {

		final String xhtml = convertMdToXHTML(markdownContent);

		// System.out.println(xhtml);

		// System.out.println("------------------");

		final StringWriter writer = new StringWriter();

		final Reader xmlReader = new StringReader("<div>" + xhtml + "</div>");

		final InputStream xsltIs = Thread.currentThread()
				.getContextClassLoader()
				.getResourceAsStream("xslt/md2confluence.wiki.xsl");
		try {

			final Reader xsltReader = new InputStreamReader(xsltIs, UTF_8);

			TransformUtils.transform(new StreamSource(xsltReader),
					new StreamSource(xmlReader), new StreamResult(writer));

		} finally {
			xsltIs.close();
		}

		final String converted = writer.toString();

		System.out.println(converted);

		return converted;
	}

	/**
	 * Convert a Markdown stream into a XHTML stream, using txtmark.
	 */
	public static String convertMdToConfluenceMarkup(final File markdownFile)
			throws IOException, TransformerException {

		final String markdownContent = FileUtils.readFileToString(markdownFile,
				UTF_8);

		return convertMdToConfluenceMarkup(markdownContent);
	}

	public static void convertMdToConfluenceMarkup(final File markdownInFile,
			final File wikiOutFile) throws IOException, TransformerException {

		final String wiki = convertMdToConfluenceMarkup(markdownInFile);

		FileUtils.write(wikiOutFile, wiki, UTF_8);
	}

	/**
	 * Convert a bunch of Markdown files into a single HTML site.
	 */
	public static void convertMdToHTMLSite(final File destDir,
			final File... sourceFiles) throws IOException, TransformerException {

		final File rootDir = lookForRootDir(sourceFiles);

		final MarkdownFiles mdFiles = loadMarkdownFiles(rootDir, sourceFiles);

		final MarkdownToHTMLConverter converter = new MarkdownToHTMLConverter();

		converter.convertToHTMLSite(mdFiles, destDir);
	}

	private static File lookForRootDir(final File[] sourceFiles)
			throws IOException {

		File rootDir = null;

		for (final File sourceFile : sourceFiles) {

			if (sourceFile.isHidden()) {

				continue;

			} else if (sourceFile.isFile()
					&& sourceFile.getName().endsWith(".md")) {

				if (rootDir == null) {

					rootDir = sourceFile.getParentFile();

				} else {

					throw new NotImplementedException();
					// final String parentPath =
					// sourceFile.getParentFile().getCanonicalPath();
				}

			} else if (sourceFile.isDirectory()) {

				final String path = sourceFile.getCanonicalPath();

				if (!path.startsWith(rootDir.getCanonicalPath())) {
					throw new NotImplementedException("path: " + path
							+ ", rootDir: " + rootDir.getCanonicalPath());
				}
			}
		}

		if (rootDir == null) {
			throw new IllegalStateException("rootDir should not be null");
		}

		return rootDir;
	}

	private static MarkdownFiles loadMarkdownFiles(final File rootDir,
			final File[] sourceFiles) throws IOException {

		final MarkdownFiles mdFiles = new MarkdownFiles(rootDir);

		for (final File sourceFile : sourceFiles) {

			if (sourceFile.isHidden()) {

				continue;

			} else if (sourceFile.isFile()
					&& sourceFile.getName().endsWith(".md")) {

				mdFiles.addFile(sourceFile);

			} else if (sourceFile.isDirectory()) {

				mdFiles.addDirectory(sourceFile);
			}
		}

		mdFiles.declareBlogEntriesFileNamePrefixDateFormat("blog/", "yyyyMMdd");

		return mdFiles;
	}

	/**
	 * Convert a bunch of Markdown files into a single HTML site with frames.
	 */
	public static void convertMdToHTMLFrameset(final File destDir,
			final File... sourceFiles) throws IOException, TransformerException {

		final File rootDir = lookForRootDir(sourceFiles);

		final MarkdownFiles mdFiles = loadMarkdownFiles(rootDir, sourceFiles);

		final MarkdownToHTMLFramesetConverter converter = new MarkdownToHTMLFramesetConverter();

		converter.convertToHTMLSite(mdFiles, destDir);
	}
}
