package net.avcompris.tools.md;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avcompris.lang.NotImplementedException;

public class MarkdownFiles {

	public MarkdownFiles(final File rootDir) throws IOException {

		// this.rootDir =
		checkNotNull(rootDir, "rootDir");

		rootDirPath = rootDir.getCanonicalPath();
	}

	private final String rootDirPath;
	// private final File rootDir;

	private final Map<String, FileDesc> fileDescs = new HashMap<String, FileDesc>();

	public void addFile(final File mdFile) throws IOException {

		checkNotNull(mdFile, "mdFile");

		if (!mdFile.isFile()) {
			throw new FileNotFoundException("mdFile should be a file: "
					+ mdFile.getCanonicalPath());
		}

		final String mdFileName = mdFile.getName();

		if (!mdFileName.endsWith(".md")) {
			throw new IllegalArgumentException(
					"Filename should end with \".md\", but was: "
							+ mdFile.getCanonicalPath());
		}

		final FileDesc fileDesc = new FileDesc(mdFile);

		fileDescs.put(fileDesc.path, fileDesc);
	}

	public void addDirectory(final File dir) throws IOException {

		checkNotNull(dir, "dir");

		if (!dir.isDirectory()) {
			throw new FileNotFoundException("dir should be a directory: "
					+ dir.getCanonicalPath());
		}

		for (final File file : dir.listFiles()) {

			if (file.isHidden()) {

				continue;

			} else if (file.isFile() && file.getName().endsWith(".md")) {

				addFile(file);

			} else if (file.isDirectory()) {

				addDirectory(file);
			}
		}
	}

	public Iterable<FileDesc> getDescs() {

		return fileDescs.values();
	}

	public FileDesc getDesc(final String path) {

		checkNotNull(path, "path");

		final FileDesc fileDesc = fileDescs.get(path);

		if (fileDesc == null) {
			throw new IllegalArgumentException("No fileDesc with path: " + path);
		}

		return fileDesc;
	}

	public boolean hasDesc(final String path) {

		checkNotNull(path, "path");

		final FileDesc fileDesc = fileDescs.get(path);

		return (fileDesc != null);
	}

	public class FileDesc {

		private FileDesc(final File originalMarkdownFile) throws IOException {

			this.originalMarkdownFile = checkNotNull(originalMarkdownFile,
					"originalMarkdownFile");

			final String fullPath = originalMarkdownFile.getCanonicalPath();

			mdFileName = originalMarkdownFile.getName();

			if (!mdFileName.endsWith(".md")) {
				throw new IllegalArgumentException(
						"filename should end with \".md\", but was: "
								+ fullPath);
			}
			
			if (mdFileName.contains(" ")) {
				throw new IllegalArgumentException(
						"filename should not contain space \" \", but was: "
								+ fullPath);
			}

			if (!fullPath.startsWith(rootDirPath)) {
				throw new IllegalArgumentException(
						"fullPath should start with rootDir: \"" + rootDirPath
								+ "\", but was: " + fullPath);
			}

			final String path = fullPath.substring(rootDirPath.length());

			if (!path.startsWith("/")) {
				throw new IllegalStateException(
						"path should start with \"/\", but was: " + path);
			}

			this.path = path.substring(1);

			setHtmlOutputPath(this.path.substring(0,
					path.length() - ".md".length() - 1).replace(" ", "_")
					+ ".html");
		}

		public final String path;
		public final String mdFileName;
		public final File originalMarkdownFile;

		private final List<Content> additionalContents = new ArrayList<Content>();

		public FileDesc appendContent(final String content) {

			checkNotNull(content, "content");

			additionalContents.add(new StringContent(content));

			return this;
		}

		public FileDesc appendToc() {

			additionalContents.add(new TocContent());

			return this;
		}

		public void setHtmlOutputPath(final String htmlOutputPath) {

			this.htmlOutputPath = checkNotNull(htmlOutputPath, "htmlOutputPath");

			for (final char c : htmlOutputPath.toCharArray()) {

				if ((c >= '0' && c <= '9') //
						|| (c >= 'A' && c <= 'Z') //
						|| (c >= 'a' && c <= 'z')) {
					continue;
				}

				switch (c) {
				case '/':
				case '.':
				case '-':
				case '_':
				case ',':
					break;
				default:
					throw new IllegalArgumentException(
							"Illegal character in htmlOutputPath: " + c + " ("
									+ (int) c + ")");
				}
			}
		}

		private String htmlOutputPath;

		public String getHtmlOutputPath() {

			if (htmlOutputPath == null) {
				throw new IllegalStateException(
						"htmlOutputPath should not be null");
			}

			return htmlOutputPath;
		}

		public boolean isBlogEntry() {

			throw new NotImplementedException();
		}
	}

	private interface Content {

	}

	private static class StringContent implements Content {

		public StringContent(final String content) {

			this.content = checkNotNull(content, "content");
		}

		public final String content;
	}

	private static class TocContent implements Content {

	}

	private final Map<String, String> blogEntriesFileNamePrefixDateFormats = new HashMap<String, String>();

	public void declareBlogEntriesFileNamePrefixDateFormat(final String dirPath, final String dateFormat) {

		checkNotNull(dirPath, "dirPath");
		checkNotNull(dateFormat, "dateFormat");

		if (!"yyyyMMdd".equals(dateFormat)) {
			throw new NotImplementedException("dateFormat: " + dateFormat);
		}

		blogEntriesFileNamePrefixDateFormats.put(dirPath, dateFormat);
	}
	
	public boolean isBlogEntries(final String dirPath) {
		
		checkNotNull(dirPath, "dirPath");

		return blogEntriesFileNamePrefixDateFormats.containsKey(dirPath);
	}
	
	public String getBlogEntriesFileNamePrefixDateFormats(final String dirPath) {
		
		return blogEntriesFileNamePrefixDateFormats.get(dirPath);
	}
}
