package net.avcompris.tools.md;

import java.io.IOException;
import java.io.InputStream;

public class MarkdownToHTMLConverter extends AbstractMarkdownToXxxConverter {

	public MarkdownToHTMLConverter() {
		
		super(false);
	}

	@Override
	protected InputStream getDefaultXSLTStream() throws IOException {

		return getResourceAsStream("xslt/default-md2site.html.xsl");
	}
}
