package net.avcompris.tools.md;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.avcompris.domdumper.Dumper;
import net.avcompris.domdumper.XMLDumpers;
import net.avcompris.tools.md.MarkdownFiles.FileDesc;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.util.TransformUtils;

abstract class AbstractMarkdownToXxxConverter {

	protected AbstractMarkdownToXxxConverter(final boolean isFrameset) {

		this.isFrameset = isFrameset;
	}

	private final boolean isFrameset;

	public final void setTransformationSheet(final File xsltFile)
			throws IOException {

		this.xsltFile = checkNotNull(xsltFile, "xsltFile");
	}

	public final void setCssStyleheet(final File cssFile) throws IOException {

		this.cssFile = checkNotNull(cssFile, "cssFile");
	}

	public final void setMenuHeader(final String menuHeader) {

		this.menuHeader = checkNotNull(menuHeader, "menuHeader");
	}

	@Nullable
	private File xsltFile = null;

	@Nullable
	private File cssFile = null;

	@Nullable
	private String menuHeader = null;

	public final void convertToHTMLSite(final MarkdownFiles mdFiles,
			final File htmlOutDir) throws IOException, TransformerException {

		// 1. Sanity checks

		final Set<String> htmlOutputPaths = new HashSet<String>();

		for (final FileDesc fileDesc : mdFiles.getDescs()) {

			final String htmlOutputPath = fileDesc.getHtmlOutputPath();

			if (htmlOutputPaths.contains(htmlOutputPath)) {
				throw new IllegalStateException("Duplicate htmlOutputPath: "
						+ htmlOutputPath);
			}

			htmlOutputPaths.add(htmlOutputPath);
		}

		// 2. Temp XML File

		final File tempXmlFile = new File(htmlOutDir, "md2site.xml");

		FileUtils.forceMkdir(tempXmlFile.getParentFile());

		final OutputStream os = new FileOutputStream(tempXmlFile);
		try {

			final Dumper dumper = XMLDumpers
					.newDumper(
							"mdtools:pages",
							new String[] { "xmlns:mdtools=http://avcompris.net/mdtools" },
							true, os, UTF_8);

			dumper.addAttribute("date", new DateTime().toString());

			final Set<String> orderedPaths = new TreeSet<String>();

			for (final FileDesc fileDesc : mdFiles.getDescs()) {

				orderedPaths.add(fileDesc.path);
			}

			Dumper d = dumper;

			String currentDirPath = "";

			for (final String path : orderedPaths) {

				while (!path.startsWith(currentDirPath)) {

					d = d.close();

					final String subPath = currentDirPath.substring(0,
							currentDirPath.length() - 1);

					if (!subPath.contains("/")) {

						currentDirPath = "";

					} else {

						final int index = subPath.lastIndexOf("/");

						if (index == -1) {
							throw new IllegalStateException("path: " + path
									+ ", currentDirPath: " + currentDirPath);
						}

						currentDirPath = currentDirPath.substring(0, index + 1);
					}
				}

				for (String p = path.substring(currentDirPath.length());;) {

					final int index = p.indexOf("/");

					if (index == -1) {
						break;
					}

					final String dirName = p.substring(0, index);

					currentDirPath += dirName + "/";

					d = d.addElement("mdtools:directory").addAttribute("name",
							dirName);

					if (mdFiles.isBlogEntries(currentDirPath)) {

						d.addAttribute("blogEntries", true);

						d.addAttribute(
								"fileNamePrefixDateFormat",
								mdFiles.getBlogEntriesFileNamePrefixDateFormats(currentDirPath));
					}

					p = p.substring(index + 1);
				}

				final FileDesc fileDesc = mdFiles.getDesc(path);

				System.out.println("Reading " + fileDesc.path + "...");

				final Dumper page = d
						.addElement("mdtools:page")
						.addAttribute("path", fileDesc.path)
						.addAttribute("mdFileName", fileDesc.mdFileName)
						.addAttribute("htmlOutputPath",
								fileDesc.getHtmlOutputPath());

				final String xhtmlContent = translateAHrefs(
						MarkdownUtils.convertMdToXHTML( //
								fileDesc.originalMarkdownFile), fileDesc,
						mdFiles);

				page.addRawCharacters(xhtmlContent);

				page.close();
			}

			dumper.close();

		} finally {
			os.close();
		}

		// 3. Transform into HTML

		final Properties params = new Properties();

		if (menuHeader != null) {
			params.setProperty("menuHeader", menuHeader);
		}

		if (isFrameset) {

			params.setProperty("frametype", "frameset");

			transform(tempXmlFile, params, new File(htmlOutDir, "index.html"));

			params.setProperty("frametype", "menu");

			transform(tempXmlFile, params, new File(htmlOutDir,
					"menu_frame.html"));

			params.setProperty("frametype", "page");
		}

		for (final FileDesc fileDesc : mdFiles.getDescs()) {

			final String htmlOutputPath = fileDesc.getHtmlOutputPath();

			if (isFrameset
					&& (htmlOutputPath.equals("index.html") || htmlOutputPath
							.equals("menu_frame.html"))) {
				throw new IllegalStateException(
						"Illegal htmlOutputPath for context \"isFrameset\": "
								+ htmlOutputPath);
			}

			System.out.println("Generating " + htmlOutputPath + "...");

			final File htmlOutputFile = new File(htmlOutDir, htmlOutputPath);

			FileUtils.forceMkdir(htmlOutputFile.getParentFile());

			params.setProperty("path", fileDesc.path);

			transform(tempXmlFile, params, htmlOutputFile);
		}

		// 4. CSS File

		if (cssFile == null) {

			final InputStream is = getResourceAsStream("css/default-md2site.css");
			try {

				FileUtils.writeByteArrayToFile(new File(htmlOutDir,
						"styles.css"), IOUtils.toByteArray(is));

			} finally {
				is.close();
			}
		}
	}

	private void transform(final File tempXmlFile, final Properties params,
			final File htmlOutputFile) throws IOException, TransformerException {

		final InputStream xmlIs = new FileInputStream(tempXmlFile);
		try {

			final Reader xmlReader = new InputStreamReader(xmlIs, UTF_8);

			final InputStream xsltIs = (xsltFile == null) ? getDefaultXSLTStream()
					: new FileInputStream(xsltFile);
			try {

				final Reader xsltReader = new InputStreamReader(xsltIs, UTF_8);

				final OutputStream htmlOs = new FileOutputStream(htmlOutputFile);
				try {

					final Writer htmlWriter = new OutputStreamWriter(htmlOs,
							UTF_8);

					TransformUtils.transform(new StreamSource(xsltReader),
							params, new StreamSource(xmlReader),
							new StreamResult(htmlWriter));

				} finally {
					htmlOs.close();
				}

			} finally {
				xsltIs.close();
			}

		} finally {
			xmlIs.close();
		}
	}

	protected abstract InputStream getDefaultXSLTStream() throws IOException;

	protected static InputStream getResourceAsStream(final String path)
			throws IOException {

		final File file = new File("src/main/resources", path);

		if (file.isFile()) {
			return new FileInputStream(file);
		}

		final InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(path);

		if (is == null) {
			throw new FileNotFoundException("Cannot find resource: " + path);
		}

		return is;
	}

	protected static String translateAHrefs(final String xhtmlContent,
			final FileDesc fileDesc, final MarkdownFiles mdFiles)
			throws IOException {

		final String dirPath = fileDesc.path.contains("/") ? (substringBeforeLast(
				fileDesc.path, "/") + "/") : "";

		String s = xhtmlContent;

		for (int offset = 0;;) {

			final int index = s.indexOf("href=\"", offset);

			if (index == -1) {
				break;
			}

			final int end = s.indexOf("\"", index + 6);

			final String href = s.substring(index + 6, end);

			offset = index + 1;

			if (!href.endsWith(".md")) {
				continue;
			}

			final String hrefPath = resolvePath(dirPath, href);

			if (!mdFiles.hasDesc(hrefPath)) {
				throw new FileNotFoundException(
						"Cannot find .md target file for href: " + href
								+ ", in file: " + fileDesc.path);
			}

			final FileDesc hrefDesc = mdFiles.getDesc(hrefPath);

			final String basedir = repeat("../",
					countMatches(fileDesc.getHtmlOutputPath(), "/"));

			s = s.substring(0, index + 6) + basedir
					+ hrefDesc.getHtmlOutputPath() + "\" mdtools:path=\""
					+ hrefDesc.path + s.substring(end);
		}

		return s;
	}

	protected static String resolvePath(final String dirPath, final String href) {

		if (isBlank(dirPath)) {
			return href.startsWith("./") ? href.substring(2) : href;
		}

		if (href.startsWith("./")) {
			return dirPath + href.substring(2);
		}

		String p = dirPath;
		String h = href;

		while (h.startsWith("../")) {

			if (!p.contains("/")) {
				throw new RuntimeException("dirPath: " + dirPath
						+ " and href: " + href + " do not match");
			}

			p = substringBeforeLast(p, "/");

			p = p.contains("/") ? (substringBeforeLast(p, "/") + "/") : "";
			h = h.substring(3);
		}

		return p + h;
	}
}
