<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:mdtools="http://avcompris.net/mdtools">
<xsl:output method="html" indent="no" encoding="UTF-8" doctype-public="HTML"/>

<xsl:param name="path" select="'README.md'"/>
<xsl:param name="menuHeader"/>

<xsl:variable name="basedir">
	<xsl:call-template name="basedir"/>
</xsl:variable>

<xsl:variable name="links" select="/mdtools:pages//mdtools:page[@path = $path]
	/descendant::a[@mdtools:path]"/>

<xsl:template match="/mdtools:pages//mdtools:page[@path = $path]">

<xsl:variable name="page-title">
<xsl:call-template name="page-title"/>
</xsl:variable>

<html>
<head>
<title>
	<xsl:value-of select="$page-title"/>
</title>
<link type="text/css" rel="stylesheet" href="{$basedir}styles.css"/>
</head>
<body id="body-{translate($path, '/.,', '___')}">

<div id="div-menu">
<ul>
	<xsl:if test="not(normalize-space($menuHeader) = '')">
		<xsl:value-of disable-output-escaping="yes" select="$menuHeader"/>
	</xsl:if>
	<xsl:apply-templates select="/mdtools:pages/*" mode="toc"/>
</ul>
</div>

<div id="div-body">

<xsl:apply-templates mode="content"/>

<xsl:variable name="pages-linkedFromHere"
	select="//mdtools:page[@path = $links/@mdtools:path]"/>
<xsl:variable name="pages-linkingToHere"
	select="//mdtools:page[descendant::a/@mdtools:path = $path]"/>

<xsl:if test="$pages-linkedFromHere or $pages-linkingToHere">
<div id="div-relatedPages">
<h2>Pages apparentées</h2>
<ul>

<xsl:apply-templates select="/mdtools:pages/*" mode="relatedPages"/>

</ul>
</div>
</xsl:if>

</div>

<div id="div-footer">
Generated on: <xsl:value-of
	select="translate(substring(/mdtools:pages/@date, 1, 19), 'T', ' ')"/>
</div>

</body>
</html>

</xsl:template>

<xsl:template match="mdtools:page | text()"/>

<xsl:template name="basedir">
<xsl:param name="path"
	select="/mdtools:pages//mdtools:page[@path = $path]/@htmlOutputPath"/>

<xsl:if test="contains($path, '/')">
	<xsl:text>../</xsl:text>
	<xsl:call-template name="basedir">
		<xsl:with-param name="path" select="substring-after($path, '/')"/>
	</xsl:call-template>
</xsl:if>

</xsl:template>

<xsl:template match="mdtools:page" mode="toc">
<li>
<xsl:attribute name="class">
	<xsl:text>file</xsl:text>
	<xsl:if test="@path = $path"> selected</xsl:if>
</xsl:attribute>
	<a href="{$basedir}{@htmlOutputPath}">
		<xsl:call-template name="page-title"/>
	</a>
</li>
</xsl:template>

<xsl:template match="mdtools:directory" mode="toc">
<xsl:choose>
<xsl:when test="@blogEntries = 'true'">
<li class="directory blogEntries">
	<div class="label">
	<xsl:value-of select="@name"/>
	</div>
	<ul>
		<xsl:apply-templates select="*" mode="toc">
		<xsl:sort select="@mdFileName" order="descending"/>
		</xsl:apply-templates>
	</ul>
</li>
</xsl:when>
<xsl:otherwise>
<li class="directory">
	<div class="label">
	<xsl:value-of select="@name"/>
	</div>
	<ul>
		<xsl:apply-templates select="*" mode="toc"/>
	</ul>
</li>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="mdtools:page" mode="relatedPages">
<xsl:if test="@path = $links/@mdtools:path or descendant::a/@mdtools:path = $path">
<li>
	<a href="{$basedir}{@htmlOutputPath}">
		<xsl:call-template name="page-title"/>
	</a>
</li>
</xsl:if>
</xsl:template>

<xsl:template match="mdtools:directory" mode="relatedPages">
<xsl:choose>
<xsl:when test="@blogEntries = 'true'">
	<xsl:apply-templates select="*" mode="relatedPages">
	<xsl:sort select="@mdFileName" order="descending"/>
	</xsl:apply-templates>
</xsl:when>
<xsl:otherwise>
	<xsl:apply-templates select="*" mode="relatedPages"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="page-title">
<xsl:if test="ancestor::mdtools:directory/@blogEntries = 'true'">
	<xsl:value-of select="concat(
		substring(@mdFileName, 1, 4), '-',
		substring(@mdFileName, 5, 2), '-',
		substring(@mdFileName, 7, 2), ' — ')"/>
</xsl:if>
<xsl:choose>
<xsl:when test="h1">
	<xsl:value-of select="h1[1]"/>	
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="@mdFileName"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="*" mode="content">

<xsl:copy>
<xsl:copy-of select="@*"/>

<xsl:apply-templates mode="content"/>

</xsl:copy>

</xsl:template> 

<xsl:template match="mdtools:page//a[@mdtools:path]" mode="content">

<xsl:variable name="path" select="@mdtools:path"/>

<xsl:copy>
<xsl:copy-of select="@*[not(name() = 'mdtools:path')]"/>

<xsl:for-each select="//mdtools:page[@path = $path]">
<xsl:call-template name="page-title"/>
</xsl:for-each>

</xsl:copy>

</xsl:template> 

</xsl:stylesheet>