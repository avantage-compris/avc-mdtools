<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:mdtools="http://avcompris.net/mdtools">
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<xsl:variable name="LINEBREAK">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="/*">

<xsl:apply-templates/>

</xsl:template>

<xsl:template match="h1 | h2 | h3 | h4 | h5 | h6">

<xsl:if test="preceding-sibling::*">
<xsl:copy-of select="$LINEBREAK"/>
<xsl:copy-of select="$LINEBREAK"/>
</xsl:if>

<xsl:value-of select="concat(name(), '. ', normalize-space(.))"/>

<!-- <xsl:copy-of select="$LINEBREAK"/> -->

</xsl:template> 

<xsl:template match="p">

<xsl:if test="preceding-sibling::*">
	<xsl:if test="not(preceding-sibling::*[1]/self::table)">
		<xsl:copy-of select="$LINEBREAK"/>
	</xsl:if>
	<xsl:copy-of select="$LINEBREAK"/>
</xsl:if>

<xsl:apply-templates/>

</xsl:template>

<xsl:template match="a[@href]">

<xsl:text>[</xsl:text>
<xsl:value-of select="normalize-space(.)"/>
<xsl:text>|</xsl:text>
<xsl:value-of select="@href"/>
<xsl:text>]</xsl:text>

</xsl:template>

<xsl:template match="text()">

<xsl:if test="not(normalize-space(.) = '')">
<xsl:value-of select="translate(., '&#10;&#13;', '  ')"/>
</xsl:if>

</xsl:template>

<xsl:template match="*">

<xsl:message terminate="yes">Unknown XHTML tag: <xsl:value-of
	select="name()"/>
</xsl:message>

</xsl:template>

<xsl:template match="pre">

<xsl:copy-of select="$LINEBREAK"/>
<xsl:copy-of select="$LINEBREAK"/>

<xsl:text>{code}</xsl:text>

<xsl:copy-of select="$LINEBREAK"/>

<xsl:copy-of select="code/text()"/>

<xsl:text>{code}</xsl:text>

<!-- <xsl:copy-of select="$LINEBREAK"/> -->

</xsl:template>

<xsl:template match="code">
<xsl:value-of select="concat('{{', ., '}}')"/>
</xsl:template>

<xsl:template match="em">
<xsl:value-of select="concat('_', ., '_')"/>
</xsl:template>

<xsl:template match="strong">
<xsl:value-of select="concat('*', ., '*')"/>
</xsl:template>

<xsl:template match="table">

<xsl:copy-of select="$LINEBREAK"/>
<xsl:copy-of select="$LINEBREAK"/>

<xsl:for-each select="thead/tr">
<xsl:for-each select="th">
<xsl:if test="preceding-sibling::*">
<xsl:text> </xsl:text>
</xsl:if>
<xsl:value-of select="concat('|| ', .)"/>
</xsl:for-each>
<xsl:copy-of select="$LINEBREAK"/>
</xsl:for-each>

<xsl:for-each select="tbody/tr">
<xsl:for-each select="td">
<xsl:if test="preceding-sibling::*">
<xsl:text> </xsl:text>
</xsl:if>
<xsl:value-of select="concat('| ', .)"/>
</xsl:for-each>
<xsl:copy-of select="$LINEBREAK"/>
</xsl:for-each>

<!-- <xsl:copy-of select="$LINEBREAK"/> -->

</xsl:template>

<xsl:template match="ul">

<xsl:if test="not(parent::li)">
<xsl:copy-of select="$LINEBREAK"/>
</xsl:if>

<xsl:for-each select="li">
<xsl:call-template name="li-prefix"/>
<xsl:text>* </xsl:text>
<xsl:apply-templates/>
</xsl:for-each>

<!-- <xsl:copy-of select="$LINEBREAK"/> -->

</xsl:template>

<xsl:template name="li-prefix">

<xsl:copy-of select="$LINEBREAK"/>

<xsl:for-each select="parent::*/ancestor::ul | parent::*/ancestor::ol">
<xsl:choose>
<xsl:when test="self::ul">*</xsl:when>
<xsl:when test="self::ol">#</xsl:when>
</xsl:choose>
</xsl:for-each>

</xsl:template>

<xsl:template match="ol">

<xsl:if test="not(parent::li)">
<xsl:copy-of select="$LINEBREAK"/>
</xsl:if>

<xsl:for-each select="li">
<xsl:call-template name="li-prefix"/>
<xsl:text># </xsl:text>
<xsl:apply-templates/>
</xsl:for-each>

<!-- <xsl:copy-of select="$LINEBREAK"/> -->

</xsl:template>

<xsl:template match="br">
<xsl:copy-of select="$LINEBREAK"/>
</xsl:template>

</xsl:stylesheet>